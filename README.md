# PKGBUILDS

Tools that are not available in the Arch/AUR repositories.

* `gnome-shell-extension-autohidetopbar`

## Usage

Run this command in the same directory as the `PKGBUILD` file.

```sh
makepkg -si
```
